import React from 'react';
import 'react-native-gesture-handler';
import RootNavigator from './src/navigations/RootNavigator';
import { LogBox } from 'react-native';
LogBox.ignoreLogs(['Setting a timer']);

const App = () => {
  return(
   
     <RootNavigator />
  )
};

export default App;

  

