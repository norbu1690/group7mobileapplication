import { Image } from 'react-native'
import { Audio } from 'expo-av';
import { View, Text } from 'react-native'
import { COLORS, SIZES } from '../constants/theme';

export default data = [
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>སེམས་ཅན་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 300,height:230}} source={require('../../assets/Cat.png')}/>
                  </View>,
        options: ["ཀ་  དོམ་ཨིན།","ཁ་  ཀོཊ་ཨིན།","ག་  བྱི་ལི་ཨིན།","ང་  ཡོད་ར་མེད།"],
        correct_option: "ག་  བྱི་ལི་ཨིན།"
    },
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>སེམས་ཅན་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 380,height:230}} source={require('../../assets/dog.png')}/>
                  </View>,
        options: ["ཀ་  ་ཀ་ཤ་ཨིན།","ཁ་  རྟ་ཨིན།","ག་  ཡོད་ར་མེད།", "ང་  རོ་ཁྱི་ཨིན།"],
        correct_option: "ང་  རོ་ཁྱི་ཨིན།"
    },
    {
        question:<View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>སེམས་ཅན་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height: 300}} source={require('../../assets/cow.png')}/>
                </View>,
        // options: ["ཀ་  ཅོག་ཙེ་ཨིན།","ཁ་  སྒྲོམ་ཨིན།","ག  མེ་ཏོག་ཨིན།","ང་  སྡོད་ཁྲི་ཨིན།"],
        options: ["ཀ་  བ་ཨིན།","ཁ་  རྟ་ཨིན།","ག་  ཡོད་ར་མེད།", "ང་  རོ་ཁྱི་ཨིན།"],
        correct_option: "ཀ་  བ་ཨིན།"
    },
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>སེམས་ཅན་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height: 350}} source={require('../../assets/cock.png')}/>
                  </View>,
        options: ["ཀ་  བྱམ་ཨིན།","ཁ་  བྱི་ཅུང་ཨིན།","ག་  བྱ་པོད་ཨིན།", "ང་  ངང་པ་ཨིན།"],
        correct_option: "ག་  བྱ་པོད་ཨིན།"
    },
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>སེམས་ཅན་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height: 200}} source={require('../../assets/tiger.png')}/>
                  </View>,
        options: ["ཀ་  དོར་མ་ཨིན།","ཁ་  སྦུལ་ཨིན།","ག་  བྱི་ལི་ཨིན།","ང་  སྟག་ཨིན།"],
        correct_option: "ང་  སྟག་ཨིན།"
    }
]
