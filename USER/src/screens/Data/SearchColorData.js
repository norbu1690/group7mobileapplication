import { Image } from 'react-native'
import { Audio } from 'expo-av';
import { View, Text } from 'react-native'
import { COLORS, SIZES } from '../constants/theme';

export default data = [
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>ཚོས་གཞི་་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 300,height:280}} source={require('../../assets/yellow.png')}/>
                  </View>,
        options: ["ཀ་  དམརཔོ་ཨིན།","ཁ་  སེརཔོ་ཨིན།","ག་  ལི་ཝང་ཨིན།","ང་  ཤ་ཁ་ཨིན།"],
        correct_option: "ཁ་  སེརཔོ་ཨིན།"
    },
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>ཚོས་གཞི་་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height:280}} source={require('../../assets/red.png')}/>
                  </View>,
        options: ["ཀ་  དམརཔོ་ཨིན།","ཁ་  སེརཔོ་ཨིན།","ག་  ལི་ཝང་ཨིན།","ང་  ཤ་ཁ་ཨིན།"],
        correct_option: "ཀ་  དམརཔོ་ཨིན།"
    },
    {
        question:<View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>ཚོས་གཞི་་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height: 280}} source={require('../../assets/green.png')}/>
                </View>,
        options: ["ཀ་  དམརཔོ་ཨིན།","ཁ་  སེརཔོ་ཨིན།","ག་  ལི་ཝང་ཨིན།","ང་  ལྕང་ཁ་ཨིན།"],
        correct_option: "ང་  ལྕང་ཁ་ཨིན།"
    },
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>ཚོས་གཞི་་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height: 300}} source={require('../../assets/orange.png')}/>
                  </View>,
        options: ["ཀ་  ལྕང་ཁ་ཨིན།","ཁ་  སེརཔོ་ཨིན།","ག་  ལི་ཝང་ཨིན།","ང་  ལྕང་ཁ་ཨིན།"],
        correct_option: "ག་  ལི་ཝང་ཨིན།"
    },
    {
        question: <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
                    <Text style={{color: COLORS.white, fontSize: 25}}>ཚོས་གཞི་་འདི་ངོས་འཛིན་འབད།</Text>
                    <Image style={{width: 350,height: 350}} source={require('../../assets/black.png')}/>
                  </View>,
        options: ["ཀ་  གནགཔོ་ཨིན།","ཁ་  ལྕང་ཁ་ཨིན།","ག་  དམརཔོ་ཨིན།","ང་  ཧོན་ཚོད་ཨིན།"],
        correct_option: "ཀ་  གནགཔོ་ཨིན།"
    }
]
