import { View, Text } from 'react-native'
import React from 'react'

const Search = () => {
  return (
    <View>
      <Text>Search</Text>
    </View>
  )
}

export default Search

// import React, { useCallback, useContext, useState } from "react";
// import { StyleSheet } from "react-native";
// import { View, Text, FlatList,ScrollView, TouchableOpacity } from "react-native";
// import firebase from '../Firebase/config'
// import { useFocusEffect, useNavigation } from "@react-navigation/native";
// import { Dimensions } from "react-native";
// import { SearchBar } from 'react-native-elements';
// // import { FontAwesome5 } from '@expo/vector-icons';

// const {width,height}=Dimensions.get('window');
// const cardWidth= width/2-30;

// export default function Search({navigation}) {
//   const db = firebase.firestore();
// 	const [totalPublications, setTotalPublications] = useState(0);
// 	const [pointer, setPointer] = useState(null);
// 	const [posts, setPosts] = useState([]);
// 	const [loading, setLoading] = useState(false);
// 	const [search, setSearch] = useState('');
//  	const [filteredDataSource, setFilteredDataSource] = useState([]);

// 	useFocusEffect(
// 		useCallback(() => {
// 			db.collection("Upload")
// 				.get()
// 				.then((snap) => {
// 					setTotalPublications(snap.size);
// 				});

// 			const resultPublications = [];
// 			const filteredDataSource = [];
// 			db.collection("Article")
// 				.get()
// 				.then((snap) => {
// 					setPointer(snap.docs[snap.docs.length - 1]);
// 					snap.forEach((doc) => {
// 						resultPublications.push({ id: doc.id, ...doc.data() });
// 					});
// 					setPosts(resultPublications);
// 					setFilteredDataSource(resultPublications);
// 				});
				
// 		}, []),
// 	);

// 	const handleMore = () => {
// 		const resultPublications = [];
// 		if (posts.length < totalPublications) setLoading(true);
// 		db.collection("Upload")
// 						.get()
// 			.then((response) => {
// 				if (response.docs.length > 0) {
// 					setPointer(response.docs[response.docs.length - 1]);
// 				} else {
// 					setLoading(false);
// 				}

// 				response.forEach((doc) => {
// 					const publication = doc.data();
// 					resultPublications.push({ id: doc.id, ...publication });
// 				});
// 				setPosts([...posts, ...resultPublications]);
// 			});
// 	};
// 	const searchFilterFunction = (text) => {
// 		if (text) {
		  
// 		  const newData = posts.filter(function (item) {
// 			const itemData = item.title
// 			  ? item.title.toUpperCase()
// 			  : ''.toUpperCase();
// 			const textData = text.toUpperCase();
// 			return itemData.indexOf(textData) > -1;
// 		  });
// 		  setFilteredDataSource(newData);
// 		  setSearch(text);
// 		} else {
// 		  setFilteredDataSource(posts);
// 		  setSearch(text);
// 		}
// 	  };

// 	return (
//     <View style={{flex:1, marginTop: 40}}>
//       <View style={{flex:1,}}> 
//         <SearchBar
//           round
//           searchIcon={{size: 30}}
//           onChangeText={(text) => searchFilterFunction(text)}
//           onClear={(text) => searchFilterFunction('')}
//           placeholder="Search"
//           value={search} 
//         />
//       <View style={styles.body}>
//         {posts ? (
//           <FlatList
//             data={filteredDataSource}
//             renderItem={({item})=>{
//                           return(
//                             <View style={{ margin: 10,flexDirection:'row'}}>
//                               <View style={{ height: 50, width: '90%', borderBottomWidth: 1, borderBottomColor: '#F26349'}}>
//                                 <Text>Title: {item.title}</Text>
//                               </View>
//                               <TouchableOpacity onPress={() => {navigation.replace('HomePage', item)}}>
//                                 <View style={{width: 30,}}>
//                                   <FontAwesome5 name="eye" size={24} color="#F26349" />
//                                 </View>
//                               </TouchableOpacity>
//                             </View>
//                           )
//               }}
//             keyExtractor={(item) => item.id}
//             showsVerticalScrollIndicator={false}
//             numColumns={1}
//           />
//           ) : (
//           <Text>loading...</Text>
//         )}
        
//       </View>
//       </View>
//     </View>
// 	);
// }

// const styles = StyleSheet.create({
// 	loading: {
// 		marginTop: 10,
// 		marginBottom: 10,
// 		alignItems: "center",
// 	},
// 	icon:{
// 		marginLeft:width<380 ? 300:335,
// 		marginTop:40
// 	},
// 	head:{
// 		height:200,
// 		justifyContent:'center',
// 		alignItems:'center'
// 	},
// 	body:{
// 		flex:1,
// 		backgroundColor:'white',
// 		borderTopRightRadius:40,
// 		borderTopLeftRadius:40,
// 		padding:10,
// 		paddingBottom:0,
// 		marginTop:20
		
// 	},
// });