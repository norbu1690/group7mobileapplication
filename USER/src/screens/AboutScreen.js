import { SafeAreaView, StyleSheet, Text, View , TouchableOpacity, Image, ScrollView, Linking} from 'react-native'
import React from 'react'
import { COLORS } from './constants/theme'
import Header from '../components/Header'
import { AntDesign } from "@expo/vector-icons";

const AboutScreen = ({navigation}) => {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.RhymeView}>
          <View style={{ width: '90%', justifyContent: 'flex-start', flexDirection: 'row'}}>
            <Image style={{width:50,height:50, marginTop:5}} source={require('../assets/logo.png')}/>
            <Header>སྐོར་ལས།</Header>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity onPress={() => navigation.navigate('HomeScreen')}>
              <AntDesign name="home" size={30} color="white" />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView style={styles.scrollView}>
          <View style={styles.textView}>
            <Header>རིམ་ལུགས་ཀྱི་སྐོར།</Header>
          </View>
          <View style={styles.itemView}>
            <Image source={require('../assets/ddc.jpg')} style={{width:'60%', height:'60%'}}/>
            <Text>
              འགྲུལ་འཕྲིན་རིམ་ལུགས་འདི་ནང་ རྫོང་ཁ་གོང་འཕེལ་ལྷན་ཚོགས་ཀྱི་ཨ་ལོའི་སྤྲོ་གླུ་གི་བརྙན་འཁོར་ཚུ་ཡོདཔ་ཨིན། {'\n'} {'\u00A9'} རྫོང་ཁ་གོང་འཕེལ་ལྷན་ཚོགས། {'\n'}
              <TouchableOpacity  onPress={() => {
              Linking.openURL('https://www.dzongkha.gov.bt/');
            }}>
                <Text style={{color:'blue'}}>ddc@dzongkha.gov.bt</Text>
              </TouchableOpacity> 
            </Text>
          </View>
          <View style={styles.textView}>
            <Header>བཟོ་མི་གི་སྐོར།</Header>
          </View>
          <View style={styles.itemView}>
            <Image source={require('../assets/gcit.png')} style={{width:'60%', height:'60%'}}/>
            <Text>
              རྒྱལ་པོའི་ཞིང་བརྡ་དོན་འཕྲུལ་རིག་མཐོ་རིམ་སློབ་གྲྭ། {'\n'}ངོ་མིང། 
            </Text>
            <Text>
            ༡༽     ནོར་བུ་རྡོ་རྗེ། {'\n'}         
            ༢༽     སངས་རྒྱས་ ཚེ་རིང་། {'\n'}
            ༣༽     ཁམ་སུམ་རྐམ་དབང་མོ། {'\n'}
            ༤༽     ཨོ་རྒྱན་ སྐལ་བཟང་།
            </Text>
          </View>
          <View style={{height:100}}></View>
        </ScrollView>
      </View>
    </SafeAreaView>
  )
}

export default AboutScreen

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLORS.primary,
        height: '100%',
        marginTop: 40,
        alignItems: 'center',
        justifyContent: 'center'
      },
      RhymeView:{
        backgroundColor:'#252c4a',
        height:60,
        width:"100%",
        color: 'white',
        flexDirection: 'row' 
      },
      scrollView: {
        height: '100%',
        width: '95%',
        marginBottom: 50
      }, 
      textView: {
        backgroundColor:COLORS.tertiry, 
        marginTop:"2%",    
      },
      itemView: {
        shadowOffset: { width:2, height:2 }, 
        shadowOpacity:0.25, 
        shadowRadius:3.84, 
        elevation:10,
        backgroundColor: 'white', 
        padding: 10,
        borderRadius: 15,
        height: 340,
        width: '97%',
        margin: 5,
        alignItems: 'center',
        shadowColor:"black",
      
        
      }
})