import { StyleSheet, Text, View, SafeAreaView, Image, TouchableOpacity,FlatList, ScrollView } from 'react-native'
import Header from '../../components/Header';
import Button from '../../components/Button';
import { COLORS } from '../constants/theme';
import { AntDesign } from '@expo/vector-icons';

const Quiz = ({navigation}) => {
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.RhymeView}>
          <View style={{ width: '90%', justifyContent: 'flex-start', flexDirection: 'row'}}>
            <Image style={{width:50,height:50, marginTop:5}} source={require('../../assets/logo.png')}/>
            <Header>རིག་བསྡུར།</Header>
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity onPress={() => navigation.navigate('HomeScreen')}>
              <AntDesign name="home" size={28} color="white" />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView style={{width: '100%'}}>
        <View style={{width: '100%', alignItems: 'center', marginBottom: 100, height: '100%'}}>
          

            <TouchableOpacity style={{width: '90%'}} onPress={() => navigation.navigate('SoundAndAminal')}>
              <Button>སེམས་ཅན་ངོས་འཛིན་འབད་ནི།</Button>
            </TouchableOpacity>

            <TouchableOpacity style={{width: '90%'}} onPress={() => navigation.navigate('VegetableQuiz')}>
              <Button>ཚོད་བསྲི་ངོས་འཛིན་འབད་ནི།</Button>
            </TouchableOpacity>
            
            <TouchableOpacity style={{width: '90%'}}  onPress={() => navigation.navigate('SearchColor')}>
              <Button>ཚོས་གཞི་ངོས་འཛིན་འབད་ནི།།</Button>
            </TouchableOpacity>

            <TouchableOpacity style={{width: '90%'}} onPress={() => navigation.navigate('ColorQuiz')}>
              <Button>ཚོས་གཞི་གི་རིག་བསྡུར།</Button>
            </TouchableOpacity>
            
            <TouchableOpacity style={{width: '90%'}}  onPress={() => navigation.navigate('AnimalQuiz')}>
              <Button>སེམས་ཅན་གྱི་རིག་བསྡུར།</Button>
            </TouchableOpacity>

            <TouchableOpacity style={{width: '90%'}}  onPress={() => navigation.navigate('FruitQuiz')}>
              <Button>ཤིང་འབྲས་ཀྱི་རིག་བསྡུར།</Button>
            </TouchableOpacity>

            <TouchableOpacity style={{width: '90%'}}  onPress={() => navigation.navigate('NationalIdenties')}>
              <Button>རྒྱལ་ཡོངས་་ཀྱི་སྐོར་ལས་རིག་བསྡུར།</Button>
            </TouchableOpacity>

            {/* <TouchableOpacity style={{width: '90%'}}  onPress={() => navigation.navigate('Search')}>
              <Button>Search</Button>
            </TouchableOpacity> */}
            
        </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  )
}

export default Quiz

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.primary,
    height: '100%',
    marginTop: 40,
    alignItems: 'center'
  },
  RhymeView:{
    backgroundColor:'#252c4a',
    height:60,
    width:"100%",
    color: 'white',
    flexDirection: 'row'
  },
})