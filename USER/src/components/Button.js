import React from "react";
import { StyleSheet } from "react-native";
import { Button as PaperButton } from "react-native-paper";
import { theme } from '../core/theme';

export default function Button({ mode, style, ...props }) {
    return (
        <PaperButton 
            style={[styles.button,
            mode === 'outlined' && { backgroundColor: theme.colors.surface },
            style,
        ]}
        labelStyle={styles.text}
        mode={mode}
        {...props}/>
    )
}

const styles = StyleSheet.create({
    button: {
        width: '90%',
        marginVertical: 10,
        paddingVertical: 2,
        backgroundColor: '#252c4a',
        borderWidth:2,
        borderColor: 'black',
        alignSelf: 'center',
        borderRadius: 15
    
    },
    text: {
        fontWeight: 'bold',
        fontSize: 18,
        lineHeight: 26,
        color: 'white'
    },
})

