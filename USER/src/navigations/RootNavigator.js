import  React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import StartUp from '../screens/StartUp';
import HomeScreen from '../screens/HomeScreen';
import { Provider } from 'react-native-paper';
import { theme } from '../config/theme';
import QuizHomeScreen from '../screens/Quiz/QuizHomeScreen';
import ColorQuiz from '../screens/Quiz/ColorQuiz'
import VideoList from '../components/VideoList'
import AnimalQuiz from '../screens/Quiz/AnimalQuiz'
import AboutScreen from '../screens/AboutScreen'
import FruitQuiz from '../screens/Quiz/FruitQuiz';
import NationalIdenties from '../screens/Quiz/NationalIdentities';
import SoundAndAminal from '../screens/Quiz/SoundAndAnimal'
import VegetableQuiz from '../screens/Quiz/VegetableQuiz'
import SearchColor from '../screens/Quiz/SearchColor';
import Games from '../screens/Quiz/Games';
import Search from '../screens/Search';

const Stack = createStackNavigator();

const RootNavigator = () => {
  return (
    <Provider theme={theme}>
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="StartUp" component={StartUp} />
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="QuizHomeScreen" component={QuizHomeScreen} />
        <Stack.Screen name="VideoList" component={VideoList} />
        <Stack.Screen name="ColorQuiz" component={ColorQuiz} />
        <Stack.Screen name="AnimalQuiz" component={AnimalQuiz} />
        <Stack.Screen name='AboutScreen' component={AboutScreen} />
        <Stack.Screen name='FruitQuiz' component={FruitQuiz} />
        <Stack.Screen name='NationalIdenties' component={NationalIdenties} />
        <Stack.Screen name='SoundAndAminal' component={SoundAndAminal} />
        <Stack.Screen name='VegetableQuiz' component={VegetableQuiz} />
        <Stack.Screen name='SearchColor' component={SearchColor} />
        <Stack.Screen name='Games' component={Games} />
        <Stack.Screen name='Search' component={Search} />
        
      </Stack.Navigator>
    </NavigationContainer> 
    </Provider>
  );
};

export default RootNavigator;