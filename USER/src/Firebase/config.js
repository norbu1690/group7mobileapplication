import firebase from "firebase/compat"
import 'firebase/compat/storage'


const firebaseConfig = {

  apiKey: "AIzaSyAnQWqdnwHb0PfjDYkv1ofzalpYkS8PHBQ",
  authDomain: "nurseryrhyme-4bc26.firebaseapp.com",
  projectId: "nurseryrhyme-4bc26",
  storageBucket: "nurseryrhyme-4bc26.appspot.com",
  messagingSenderId: "831548358798",
  appId: "1:831548358798:web:432d1f8afebe7b59ef09df"
  
  // apiKey: "AIzaSyCjSrhh4rbVSrr5XmxU_hqBgrnuYrgUP8Y",
  // authDomain: "aluitrowlu-8b7d3.firebaseapp.com",
  // databaseURL: "https://aluitrowlu-8b7d3-default-rtdb.firebaseio.com",
  // projectId: "aluitrowlu-8b7d3",
  // storageBucket: "aluitrowlu-8b7d3.appspot.com",
  // messagingSenderId: "440222374537",
  // appId: "1:440222374537:web:c9821aa01912ea6cfc4458"
};
  
  firebase.initializeApp(firebaseConfig);

  const storage =  firebase.storage();

  export {storage, firebase as default}
