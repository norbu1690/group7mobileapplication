import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import { Link } from "react-router-dom";
import "./Dashboard.css";
import './PassReset.css'
import FloatingLabel from "react-bootstrap/esm/FloatingLabel";
import { useNavigate } from "react-router-dom";
import { sendPasswordResetEmail } from "firebase/auth";
import { auth } from "./firebase";

function PassReset() {
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState();
  const [error, setError] = useState("");
  const forgotPassword = async (e) => {
    e.preventDefault();
    try {
      if (email === "12200077.gcit@rub.edu.bt") {
        setMessage(
          "Password reset link has been sent to Aluitrowlus admin email. Please check your email(dont forget to check spam folder)!"
        );
        await sendPasswordResetEmail(auth, email);
      } else {
        setMessage("Please enter Aluitrowlus admin email!");
      }
    } catch (error) {
      setError(error);
    }
  };
  return (
 
    <Container className="cont">
      <h5 className="mb-3">Reset Admins Password</h5>
      <Form onSubmit={forgotPassword}>
        <FloatingLabel
       
          className="mb-5 mt-4"
          controlId="formBasicEmail"
        >
          <Form.Control
            className="inp"
            type="email"
            placeholder="    Enter admins email here"
            onChange={(event) => {
              setEmail(event.target.value);
            }}
            required
          />
        </FloatingLabel>
        {message && <p className="mb-4 text-danger">{message}</p>}
        {error && <p className="mb-4 text-danger">{error}</p>}
        <Button  className="btnlg" variant="mt-5 w-100" type="submit">
          Send Link
        </Button>

   
      
      </Form>
      <Link to="/">
        
        Click here to Login!
      </Link>
     
    </Container>

  );
}
export default PassReset;