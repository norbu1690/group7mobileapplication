import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
import { getStorage } from "firebase/storage";
import { getAuth } from "firebase/auth";


const firebaseConfig = {

  apiKey: "AIzaSyCjSrhh4rbVSrr5XmxU_hqBgrnuYrgUP8Y",
  authDomain: "aluitrowlu-8b7d3.firebaseapp.com",
  databaseURL: "https://aluitrowlu-8b7d3-default-rtdb.firebaseio.com",
  projectId: "aluitrowlu-8b7d3",
  storageBucket: "aluitrowlu-8b7d3.appspot.com",
  messagingSenderId: "440222374537",
  appId: "1:440222374537:web:c9821aa01912ea6cfc4458"

};


// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage=getStorage(app);
export const db=getFirestore(app);
export const auth=getAuth(app);
export default getFirestore();
