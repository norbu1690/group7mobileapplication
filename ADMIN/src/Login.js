import {useState} from 'react'
import { Link } from 'react-router-dom'

import './form.css'

import { auth } from './firebase'
import {useNavigate} from 'react-router-dom'
import { signInWithEmailAndPassword } from 'firebase/auth'



function Login(){

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('') 
  const [error, setError] = useState('')
  const[loading,setLoading]=useState(false);

  const navigate = useNavigate()

  const login = e => {
    e.preventDefault()
    signInWithEmailAndPassword(auth, email, password)
    .then(() => {
      setLoading(false);
      navigate('/Home')
   
    })
    .catch(err => setError(err.message))
    setLoading(false);
  }

  return(
    <div >
      <div className='auth'style={{align:'center'}}>
   
     <img src={require('./AluiTrowlu.png')}height='150' />
     <br></br>
        {error && <div className='auth__error'>{error}</div>}
        <form onSubmit={login} name='login_form' >
          <input 
          style={{width:'50%'}}
            type='email' 
            value={email}
            required
            placeholder="Enter your email"
            onChange={e => setEmail(e.target.value)}/>

          <input 
          style={{width:'50%'}}
            type='password'
            value={password}
            required
            placeholder='Enter your password'
            onChange={e => setPassword(e.target.value)}/>

          <button type='submit'
          disabled={loading}>Login</button>
        </form>
        <Link to="PassReset">
            
        forgot password?
        
        </Link>
     
       
      </div>
      
    </div>
  )
}

export default Login